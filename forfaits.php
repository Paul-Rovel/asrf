<?php 
require('actions/user/securiteAction.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include('includes/head.php') ?>
</head>
<body class="bg-color" style="background-color: #384454">
   <?php include('includes/navbar.php') ?>
<div class="container">
<br>
<div class="row row-cols-1 row-cols-md-3 g-4">
  <div class="col">
    <div class="card h-100">
      <div class="card-body">
      <h4>Forfait 8000FCFA --> 2Go</h4>
        <h5 class="card-title">Valide pendant 1mois</h5>
        <p class="card-text">Vous bénéficierez de 2Go d'espace cloud pour stocker vos données, et y accéder quand vous voudrez.</p>
        <a href="#" class="btn btn-primary">Acheter</a>
    </div>
      <div class="card-footer">
        <small class="text-muted">Dernière mise à jour 11/03/2023</small>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-100">
      <div class="card-body">
      <h4>Forfait 5000FCFA --> 1Go</h4>
        <h5 class="card-title">Valide pendant 1mois</h5>
        <p class="card-text">Vous bénéficierez de 1Go d'espace cloud pour stocker vos données, et y accéder quand vous voudrez.</p>
        <a href="#" class="btn btn-primary">Acheter</a>
    </div>
      <div class="card-footer">
        <small class="text-muted">Dernière mise à jour 11/03/2023</small>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-100">
      <div class="card-body">
      <h4>Forfait 3000FCFA --> 500Mo</h4>
        <h5 class="card-title">Valide pendant 1mois</h5>
        <p class="card-text">Vous bénéficierez de 500Mo d'espace cloud pour stocker vos données, et y accéder quand vous voudrez.</p>
        <a href="#" class="btn btn-primary">Acheter</a>
    </div>
      <div class="card-footer">
        <small class="text-muted">Dernière mise à jour 11/03/2023</small>
      </div>
    </div>
  </div>
</div>

</div>

</body>
</html>