# ASRF

ASRF est une application de sauvegarde et de restauration de fichiers développée en PHP et Bootstrap.

## Installation

Vous avez juste à telecharger le fichier zip et à l'extraire dans le dossier www de wampserver, et importer la Base de données de nom asrf.


## Comment utiliser

Vérifier que vos services de wamps soient bien démarrés,
Connectez vous au serveur a partir dun autre dispositif (tel, pc...), en entrant adresse_du_serveur/asrf,
Vous créez un compte sur la page d'inscription ou vous vous connectez, ensuite vous avez votre espace pour garder vos fichiers sur le serveur.
Reconnectez vous au compte que vous avez crée sur n'importe quel autre appareil,
Vous avez accès à tous vos fichiers sauvegardés.


## License

[MIT](https://choosealicense.com/licenses/mit/)