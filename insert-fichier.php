<!--<h1>Insersion du fichier</h1><br>
 <form method="post" action="actions/gestion/upload-fichier.php" enctype="multipart/form-data">
    <label for="mon_fichier"> Fichier (tous les formats, jusqu'à 100Mo au maximum): </label><br>
    <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
    <input type="file" name="mon_fichier" id="mon_fichier"><br>
    <input type="submit" name="submit" value="Envoyer">
</form>!-->

<!DOCTYPE html>
<html lang="en">
<?php session_start();
      include 'includes/head.php'; ?>
<body>
<?php include('includes/navbar.php') ?>

<br>
    <form class="container" method="POST" action="actions/gestion/upload-fichier.php" enctype="multipart/form-data">
       
       <?php 
            if(isset($errorMsg)){ echo '<p>'.$errorMsg.'<p>'; }
       ?>
    <h1>Insersion d'un fichier</h1><br><br>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Fichier (tous les formats sont acceptés, jusqu'à 100Mo au maximum)</label>
            <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
            <input type="file" class="form-control" name="mon_fichier" id="mon_fichier">
        </div>

        <button type="submit" class="btn btn-primary" name="submit">Enregistrer le fichier</button>
   
    </form>

    <script>src="bootstrap-4.0.0-dist/js/bootstrap.min.js" </script>
    <script>src="bootstrap-4.0.0-dist/js/jquery.js" </script>
</body>
</html>