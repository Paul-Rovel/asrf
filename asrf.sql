-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 13 mars 2023 à 15:20
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `asrf`
--
CREATE DATABASE IF NOT EXISTS `asrf` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `asrf`;

-- --------------------------------------------------------

--
-- Structure de la table `fichier`
--

DROP TABLE IF EXISTS `fichier`;
CREATE TABLE IF NOT EXISTS `fichier` (
  `id_fichier` int(255) NOT NULL AUTO_INCREMENT,
  `id_proprio` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `taille` int(255) NOT NULL,
  `emplacement` varchar(255) NOT NULL,
  `date` timestamp NOT NULL,
  PRIMARY KEY (`id_fichier`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fichier`
--

INSERT INTO `fichier` (`id_fichier`, `id_proprio`, `type`, `taille`, `emplacement`, `date`) VALUES
(1, 1, 'application/pdf', 2264167, 'cours de Cloud Computing L2_2023.pdf', '2023-03-11 01:27:27'),
(2, 1, 'application/pdf', 1108089, 'CHAP 2 ADMINISTRATION BD.pdf', '2023-03-11 01:28:27'),
(3, 1, 'video/mp4', 79918260, 'SpringBoot _ Spring Data JPA - One To Many & Join Query _ Example _ Java Techie.mp4', '2023-03-11 01:28:59'),
(4, 2, 'video/mp4', 6530108, '3 - Configure MySQL Database.mp4', '2023-03-11 01:30:28'),
(6, 2, 'image/png', 90508, 'SCREENSHOT DE LA FIGURE REALISEE SUR PACKET TRACER FOKAM.PNG', '2023-03-11 01:31:49'),
(7, 3, 'application/pdf', 173330, 'TIC ET MANAGEMENT.pdf', '2023-03-11 01:39:36'),
(8, 3, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 169301, 'TRABAJO  MATEMATICA DISCRETA PAUL.docx', '2023-03-11 01:40:59'),
(9, 3, 'application/octet-stream', 906986, 'Merise.rar', '2023-03-11 01:42:01'),
(10, 1, 'video/mp4', 2669619, 'VID-20230213-WA0002.mp4', '2023-03-11 02:37:03'),
(11, 1, 'application/x-gzip', 5781101, 'php_manual_fr.html.gz', '2023-03-11 06:59:07'),
(12, 1, 'application/x-zip-compressed', 27156934, 'php-8.2.3-src.zip', '2023-03-11 07:00:18'),
(14, 1, 'video/mp4', 6476937, 'VID_20230311_080625.mp4', '2023-03-11 07:06:33'),
(15, 1, 'image/jpeg', 3682815, '16785255044715383473230709398415.jpg', '2023-03-11 09:05:16'),
(16, 1, 'video/mp4', 65214773, '@Anime_Canal Fumetsu No Anata Ep06 VOSTFR.mp4', '2023-03-11 09:06:15'),
(18, 2, 'video/mp4', 62865090, '@AnimeCanal Blue Lock Ep14 VOSTFR.mp4', '2023-03-11 20:30:03'),
(21, 4, 'image/jpeg', 205105, '1678571661397.jpg', '2023-03-12 08:17:23'),
(23, 1, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 47988, 'expose tic nv.docx', '2023-03-12 16:30:09'),
(27, 6, 'image/jpeg', 121975, 'IMG-20221222-WA0004~4.jpg', '2023-03-12 18:59:53'),
(28, 6, 'video/mp4', 5890991, 'VID_20230312_200047.mp4', '2023-03-12 19:00:59'),
(29, 6, 'application/pdf', 690477, '18 Emploi de temps du 13 au 18 Mars  2023.pdf', '2023-03-12 19:02:19'),
(31, 1, 'application/pdf', 624879, 'Fiche technique ASRF paulkamga.pdf', '2023-03-13 00:50:28'),
(32, 1, 'image/jpeg', 2974998, '16786838014625132743289025746919.jpg', '2023-03-13 05:06:29'),
(33, 7, 'video/quicktime', 4571672, 'trim.C45D8FE0-DB1C-4F78-AD6B-FC3FBE3EDA31.MOV', '2023-03-13 06:27:09'),
(34, 8, 'image/jpeg', 120171, '313748605_6071742159515120_4012059114004425435_n.jpg', '2023-03-13 06:31:26'),
(36, 1, 'application/pdf', 110612, 'chap5__1144243450200.pdf', '2023-03-13 13:51:11');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(255) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `tel` bigint(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id_user`, `pseudo`, `nom`, `prenom`, `tel`, `mdp`) VALUES
(1, 'ShadowX', 'Fokam Kamga', 'Paul Rovel', 650239073, '$2y$10$9dOv5GIziVdA79bZm4J41ukZyAc5t10uTVOcnNPi.K9xaMnxeTNC.'),
(2, 'Greg', 'Bobo', 'Benoa', 8566666, '$2y$10$BVyOGY/NRR7W2zQOiCb85O5yu.XljQ/HSjSZLDg6ZbAKZQm.Tp9Am'),
(3, 'Haba', 'Haba', 'Haba', 5542, '$2y$10$Y.MYiZ1hl2Eppghd8zOWpue2vB413bZAKC2t/1yxnz3b.Q/Xzb9na'),
(4, 'Guardian', 'YEBGA', 'Brayan ', 690026297, '$2y$10$IEEMC5khjzPuQ8Jw5Cb4VOA5KAA3uznUcG6a6Va1VpXr5QlnV5TZq'),
(5, 'philosophe', 'yebga', 'brayan', 690026297, '$2y$10$PXpThaKubb4FRjksQUu7RumtsEgKaeGKf7GdnNsw0n1OiF/M9je76'),
(6, 'Ariri', 'Otabela atangana', 'Joël ariel', 655237124, '$2y$10$Gn9VNj3eckILTJMeJGaQzuGLMjYAZ719OdgUedAVVUCpyxdCS/J0m'),
(7, 'Yatabare ', 'Edwin ', 'Alahn', 696715079, '$2y$10$/5cEIC3O9wVRdXyaXgBvQ.s7UpI636JN8M7r7KvGYdk8HfezHlv6a'),
(8, 'azert', 'azert', 'azert', 2345678, '$2y$10$4r.0yn80JE9aDeXq5uRIUupP.J3HuiTmVOIkiexkXMPvxPizIwO.a'),
(9, 'Ali', 'Ali', 'El', 699144137, '$2y$10$cFYjewdYCpxZhdPfLkrFf.sivCMuRCqQa1M58Oynjfxm0ypX/gmui'),
(10, 'Paul', 'Kamga', 'Rovel', 650239073, '$2y$10$749qeM52sJ6p.oSywe1Ygu2g95woYetEUrRpKklWQX9P8CTciSz7m');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
