<?php 
require('actions/user/securiteAction.php');
require('actions/affichage/accueilAction.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include('includes/head.php') ?>
</head>
<body style="background-color: #384454">
   <?php include('includes/navbar.php') ?>
<div class="bg-color" style="background-color: #384454">
   <div class="container"><br>
      <h4><font color="#f542b0"> Bienvenue, <?= $_SESSION['pseudo'];?>.</font></h4>
      <br>
         <div class="container">
            <form method="GET">
            <div class="form-group row">
               <div class="col-8">
                  <input type="search" name="search" class="form-control" placeholder="Rechercher" aria-label="Search">
               </div>
               <div class="col-4">
                  <button class="btn btn-success" type="submit">Rechercher</button>
               </div>
            </div>
            </form>
         </div>

   <br><br>
   <a class="btn btn-primary" href="insert-fichier.php" role="button">Ajouter un fichier</a>
      <?php
         while($file=$getfiles->fetch()){            
            ?>
            <br><br>
            <div class="card" style="width: 21.3rem;">
            <?php
            $typ=strrchr($file['type'], "/");
            $t=str_replace('/', '', $typ);
            if($t=="png"||$t=="jpeg"||$t=="jpg"||$t=="gif"||$t=="tif"||$t=="psd"){//cas des images?>
                  <img src="uploads/<?=$file['emplacement']?>" class="card-img-top" alt="...">

            <?php }elseif($t=="mp4"||$t=="m4v"||$t=="mov"||$t=="qt"||$t=="avi"||$t=="flv"||$t=="mkv"||$t=="asf"||$t=="ts"||$t=="dat"||$t=="vob"||$t=="quicktime"){//cas des videos ?>
                  <video width="339" height="250" controls>
                  <source src="uploads/<?=$file['emplacement']?>" type=<?= $file['type']?>>
                  </video>
            <?php }else if($t=="x-zip-compressed"){?>
               <img src="uploads/imagezip" class="card-img-top" alt="...">
            <?php }else if($t=="pdf"){?>
               <img src="uploads/imgextensionpdf" class="card-img-top" alt="...">
            <?php }else if($t=="vnd.openxmlformats-officedocument.wordprocessingml.document"){?>
               <img src="uploads/imgextensionword" class="card-img-top" alt="...">
            <?php }else if($t=="vnd.openxmlformats-officedocument.spreadsheetml.sheet"){?>
               <img src="uploads/imgextensionexcel" class="card-img-top" alt="...">
            <?php }else{?>
               <img src="uploads/imagedesfihiersquisontinconnus.png" class="card-img-top" alt="...">
            <?php }
            ?>
                  <div class="card-body">
                     <h5 class="card-title">
                        Nom du fichier: <?= $file['emplacement']; ?>
                     </h5>
                     <h5 class="card-title">
                        <?php if($file['taille']<(1024*1024)){ ?>
                     Taille: <?= round(($file['taille'])/1024, 2); ?>Ko
                     <?php }else{ ?>
                     Taille: <?= round(($file['taille'])/(1024*1024), 2); ?>Mo
                     <?php } ?>
                     </h5>                  
                  </div>
                  <ul class="list-group list-group-flush">
                     <li class="list-group-item"> Type: <?=$file['type']; ?></li>
                  </ul>
                  <ul class="list-group list-group-flush">
                     <li class="list-group-item"> Date: <?=$file['date']; ?></li>
                  </ul>                  
                  <div class="card-body">
                     <a href="uploads/<?=$file['emplacement']?>" class="btn btn-primary">Télécharger </a>
                     <a href="supprimerfichier.php?id=<?=$file['id_fichier'] ?>" class="btn btn-danger">Supprimer </a>
                  </div>
            </div>
            <?php

         }
      ?>
         <br><br>
   </div>
</div>

</body>
</html>