<?php 
require('actions/user/securiteAction.php');
require('actions/affichage/accueilAction.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <?php include('includes/head.php') ?>
</head>
<body style="background-color: #384454">
   <?php include('includes/navbar.php') ?>
   <div class="container">
   <br>
      <?php
        $total_fichier=0;
        $nbre_fichier=$getfiles->rowCount(); ?>
         
        <?php while($file=$getfiles->fetch()){     
            $total_fichier=$total_fichier+$file['taille'];

         }
      ?>
        
        <h3><font color="yellow">Espace total à votre disposition: 500Mo</font><font color="cafc03"> (Période d'éssai, valide jusqu'au 31/03/2023)</font></h3><br>
        <h5><font color="03a1fc">Nombre de fichiers sur le serveur: <?= $nbre_fichier; ?></font></h5><br>

        <?php if($total_fichier<(1024*1024)){ ?>
            <h5><font color="fca103">Espace occupé sur le serveur: <?= round($total_fichier/1024, 2); ?>Ko</font></h5><br>
            <h5><font color="cafc03">Espace restant sur le serveur: <?= 512000-round($total_fichier/(1024*1024), 2); ?>Ko</font></h5>
        <?php }else{ ?>
            <h5><font color="fca103">Espace occupé sur le serveur: <?= round($total_fichier/(1024*1024), 2); ?>Mo</font></h5><br>
            <h4><font color="cafc03">Espace restant sur le serveur: <?= 500-round($total_fichier/(1024*1024), 2); ?>Mo</font></h4>
        <?php } ?>
         <br><br>
   </div>

</body>
</html>