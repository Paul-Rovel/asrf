<?php 
require('actions/database.php');

$getfiles=$bdd->prepare('SELECT id_fichier, id_proprio, type, taille, emplacement, date FROM fichier where id_proprio=? ORDER BY id_fichier DESC');
$getfiles->execute(array($_SESSION['id']));


if(isset($_GET['search']) AND !empty($_GET['search'])){

    $userSearch= $_GET['search'];

    $getfiles=$bdd->prepare('SELECT id_fichier, id_proprio, type, taille, emplacement, date FROM fichier where id_proprio=? AND emplacement LIKE "%'.$userSearch.'%" ORDER BY id_fichier DESC');
    $getfiles->execute(array($_SESSION['id']));
}


?>