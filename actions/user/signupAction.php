<?php
session_start();
require('actions/database.php');

if(isset($_POST['signup'])){//Validation du formulaire ce qui se se passe si le bouton sinscrire a ete cliqué

    if(!empty($_POST['pseudo']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['tel']) && !empty($_POST['mdp'])){//si tous les champs sont renseignés

    $u_pseudo=htmlspecialchars($_POST['pseudo']);//recuperer le pseudo et stocker dans la variable u_pseudo
    $u_nom=htmlspecialchars($_POST['nom']);//recuperer le nom et stocker dans la variable u_nom
    $u_prenom=htmlspecialchars($_POST['prenom']);//meme chose pour le prenom
    $u_tel=htmlspecialchars($_POST['tel']);//meme chose pour le tel
    $u_mdp=password_hash($_POST['mdp'], PASSWORD_DEFAULT);//recuperer et crypter le mdp


    $existant= $bdd->prepare('SELECT pseudo FROM users WHERE pseudo=?');
    $existant->execute(array($u_pseudo));//recuperer les pseudo existant qui correspondent au pseudo de l'utilisateur afin de verifie rsil existe deja
   

    if($existant->rowCount()==0){// s'il nexiste aucun utilisateur avec le meme pseudo, on insere lutilisateur dans la BD
        $insertUser=$bdd->prepare('INSERT INTO users(id_user, pseudo, nom, prenom, tel, mdp) VALUES(?, ?, ?, ?, ?, ?)');
        $insertUser->execute(array(NULL, $u_pseudo, $u_nom, $u_prenom, $u_tel, $u_mdp));
        //echo('Formulaire envoyé');


        //recuperer les infos de lutilisateur
        $getInfoUreq=$bdd->prepare('SELECT id_user, pseudo, nom, prenom, tel FROM users WHERE nom=? AND prenom=? AND tel=? AND pseudo=?');
        $getInfoUreq->execute(array($u_nom, $u_prenom, $u_tel, $u_pseudo));

        $userInfos=$getInfoUreq->fetch();

        //Authentifier l'utilisateur sur le site et recuperer ses donnees dans les variables globales de session 
        $_SESSION['auth']=true;
        $_SESSION['id']=$userInfos['id_user'];
        $_SESSION['pseudo']=$userInfos['pseudo'];
        $_SESSION['nom']=$userInfos['nom'];
        $_SESSION['prenom']=$userInfos['prenom'];
        $_SESSION['tel']=$userInfos['tel'];

        //On redirige lutilisateur vers l'accueil
        header(('Location: accueil.php'));


    }else{
        $errorMsg="Ce pseudo est deja utilisé par un autre utilisateur.";
    }

}else{
    $errorMsg="Veuillez completer tous les champs...";
}

}
?>