<?php
session_start();
require('actions/database.php');

if(isset($_POST['login'])){//Validation du formulaire, ce qui se se passe si le bouton sinscrire a ete cliqué

    if(!empty($_POST['pseudo']) && !empty($_POST['mdp'])){//si tous les champs sont renseignés

    $u_pseudo=htmlspecialchars($_POST['pseudo']);//recuperer le pseudo et stocker dans la variable u_pseudo
    $u_mdp=htmlspecialchars($_POST['mdp']);//recuperer le mdp

    //verifier si pseudo existe
    $verifexist=$bdd->prepare('SELECT * FROM users WHERE pseudo=?');
    $verifexist->execute(array($u_pseudo));

    if($verifexist->rowCount()>0){//il existe des utilisateurs possedant le pseudo qui est seul
        //Recuperer les données de lutilisateur dans un tableau
        $user_Infos= $verifexist-> fetch();//avec les colonnes etant les noms des champs de la table
        
        if(password_verify($u_mdp, $user_Infos['mdp'])){

        //Authentifier l'utilisateur sur le site et recuperer ses donnees dans les variables globales de session 
        $_SESSION['auth']=true;
        $_SESSION['id']=$user_Infos['id_user'];
        $_SESSION['pseudo']=$user_Infos['pseudo'];
        $_SESSION['nom']=$user_Infos['nom'];
        $_SESSION['prenom']=$user_Infos['prenom'];
        $_SESSION['tel']=$user_Infos['tel'];

            
        header(('Location: accueil.php'));
            

        }else{
            $errorMsg="Mot de passe incorrect";
        }

    }else{
        $errorMsg="Votre pseudo est incorrect...";
    }

}else{
    $errorMsg="Veuillez completer tous les champs...";
}

}
?>